# INTERNET BANK SPRING MVC #
==========

## Instruções Back-End ##
1. Toda a documentação do projeto está no diretório 'docs'.
2. Todo o código fonte da fase atual em desenvolvimento está no diretório 'trunk'.
3. Todo o código fonte de uma ramificação específica de uma versão do software está no diretório 'branch'.
4. Todos as releases lançadas no diretório 'tags'.
